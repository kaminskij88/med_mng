import { FC } from "react";
import { Route, Switch } from "react-router-dom";
import Navbar from "./organisms/Navbar";
import Home from "./pages/Home";

const App: FC = () => {
  return (
    <>
      <Switch>
        <Navbar />
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </>
  );
};

export default App;
