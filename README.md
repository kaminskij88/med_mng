# Getting Started with Create React App

Application for managing a doctor's office

# Used technologies

- React
- Redux
- Firebase
- Styled Components
- Type Script
